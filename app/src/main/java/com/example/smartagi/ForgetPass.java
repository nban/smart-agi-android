package com.example.smartagi;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class ForgetPass extends AppCompatActivity {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myData = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);

        Button bt_Send = findViewById(R.id.bt_Send_Forget_Pass);
        Button bt_Cancel = findViewById(R.id.bt_Cancel_Forget_Pass);
        EditText inPut_Phone = findViewById(R.id.inPut_Phone_Forget_Pass);
        EditText inPut_User = findViewById(R.id.inPut_User_Forget_Pass);

        bt_Send.setOnClickListener(v -> {
            if (inPut_User.getText().length() > 0) {
                if (inPut_Phone.getText().length() == 10) {
                    myData.child(inPut_User.getText().toString()).child("Phone").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            String temp = snapshot.getValue(String.class);
                            if (temp != null){
                                if (temp.equals(inPut_Phone.getText().toString())){
                                    Random rand = new Random();
                                    Integer pass;
                                    do {
                                        pass = rand.nextInt(999999999);
                                    } while (pass < 99999);
                                    myData.child(inPut_User.getText().toString()).child("Pass").setValue(pass.toString());
                                    Notification("Mật Khẩu Mới Của Bạn Là: " + pass.toString(), pass.toString());
                                } else {
                                    Notification(R.string.text_forget_failed_phone);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Notification(error.getMessage());
                        }
                    });
                }
            }
        });

        bt_Cancel.setOnClickListener(v -> finish());

    }

    private void Notification(Integer text_Error) {
        AlertDialog.Builder Notification_Error_Dialog = new AlertDialog.Builder(this);
        Notification_Error_Dialog.setTitle(text_Error);
        Notification_Error_Dialog.setCancelable(false);
        Notification_Error_Dialog.setNegativeButton(R.string.text_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = Notification_Error_Dialog.create();
        alertDialog.show();
    }

    private void Notification(String text_Error) {
        AlertDialog.Builder Notification_Error_Dialog = new AlertDialog.Builder(this);
        Notification_Error_Dialog.setTitle(text_Error);
        Notification_Error_Dialog.setCancelable(false);
        Notification_Error_Dialog.setNegativeButton(R.string.text_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = Notification_Error_Dialog.create();
        alertDialog.show();
    }

    private void Notification(String notification, String Pass) {
        AlertDialog.Builder Notification_Dialog = new AlertDialog.Builder(this);
        Notification_Dialog.setTitle(R.string.text_title);
        Notification_Dialog.setMessage(notification);
        Notification_Dialog.setCancelable(false);
        Notification_Dialog.setNegativeButton(R.string.text_copy, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(getApplicationContext(), R.string.text_copy_done, Toast.LENGTH_SHORT).show();
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("", Pass);
                clipboard.setPrimaryClip(clip);
                dialog.cancel();
                finish();
            }
        });
        AlertDialog alertDialog = Notification_Dialog.create();
        alertDialog.show();
    }
}
