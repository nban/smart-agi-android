package com.example.smartagi

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.smartagi.ui.login.LoginActivity
import kotlinx.android.synthetic.main.fragment_account.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AccountFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AccountFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_account, container, false)
//        val database = FirebaseDatabase.getInstance()
//        val myData = database.reference
        val uSer = Singleton.getInstance().userName
        root.id_account.text = uSer
        root.bt_Logout.setOnClickListener{
            activity?.let{
//                val intent = Intent (it, LoginActivity::class.java)
//                it.startActivity(intent)
                val thisActivity: Activity? = activity
                if (thisActivity != null) {
                    startActivity(Intent(thisActivity, LoginActivity::class.java)) // if needed
                    thisActivity.finish()
                }
            }
        }
        root.bt_Chane_Pass.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_chane_pass)
        }
//        root.bt_Chane_Pass.setOnClickListener{
//            val passOld = root.pass_1.text.toString();
//            val passNew = root.pass_2.text.toString();
//            val passNewRepeat = root.pass_3.text.toString();
//            if (passOld.length > 5 && passNew.length > 5 && passNewRepeat.length > 5){
//                if(passNew == passNewRepeat){
//                    myData.child(uSer).child("Pass").addListenerForSingleValueEvent(object :
//                        ValueEventListener {
//                        override fun onDataChange(snapshot: DataSnapshot) {
//                            if (passOld == snapshot.getValue(String::class.java)){
//                                myData.child(uSer).child("Pass").setValue(passNew)
//                                Notification(R.string.pass_oke);
//                            } else {
//                                Notification(R.string.pass_failed);
//                            }
//                        }
//
//                        override fun onCancelled(error: DatabaseError) {
//                            Notification(error.message)
//                        }
//                    })
//
//                } else {
//                    Notification(R.string.text_pass_repeat_format)
//                }
//            } else {
//                Notification(R.string.invalid_password)
//            }
//        }

        return root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AccountFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AccountFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

//    private fun Notification(notification: String) {
//        val Dialog =
//            AlertDialog.Builder(requireContext())
//        Dialog.setTitle(R.string.text_title)
//        Dialog.setMessage(notification)
//        Dialog.setCancelable(false)
//        Dialog.setNegativeButton(
//            R.string.text_close
//        ) { dialog, id -> dialog.cancel() }
//        val alertDialog = Dialog.create()
//        alertDialog.show()
//    }
//
//    private fun Notification(text_Error: Int) {
//        val Dialog =
//            AlertDialog.Builder(requireContext())
//        Dialog.setTitle(text_Error)
//        Dialog.setCancelable(false)
//        Dialog.setNegativeButton(
//            R.string.text_close
//        ) { dialog, id -> dialog.cancel() }
//        val alertDialog = Dialog.create()
//        alertDialog.show()
//    }
}