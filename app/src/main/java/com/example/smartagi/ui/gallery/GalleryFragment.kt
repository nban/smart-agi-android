package com.example.smartagi.ui.gallery

import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.smartagi.R
import com.example.smartagi.Singleton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class GalleryFragment : Fragment() {

    var database = FirebaseDatabase.getInstance()
    var myData = database.reference
    var uSer = "admin"
    var Mode = "1"

    private lateinit var galleryViewModel: GalleryViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)

        val text_Temp: TextView = root.findViewById(R.id.id_temp)
        val text_Hum: TextView = root.findViewById(R.id.id_hum)
        val text_Hum_Land: TextView = root.findViewById(R.id.id_hum_land)
        val text_Light: TextView = root.findViewById(R.id.id_light)
        val text_Status_Pum: TextView = root.findViewById(R.id.id_status_pum)
        val text_Mode: TextView = root.findViewById(R.id.id_Text_Che_Do)
        val bt_Mode: Button = root.findViewById(R.id.bt_Che_Do)
        val bt_On: Button = root.findViewById(R.id.bt_Bat)
        val bt_Off:Button = root.findViewById(R.id.bt_Tat)

        uSer = Singleton.getInstance().userName

        myData.child(uSer).child("Temp").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                text_Temp.text = snapshot.getValue(String::class.java) + "°C"
            }

            override fun onCancelled(error: DatabaseError) {
                text_Temp.text = getString(R.string.text_error)
            }
        })

        myData.child(uSer).child("Hum").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                text_Hum.text = snapshot.getValue(String::class.java) + " %"
            }

            override fun onCancelled(error: DatabaseError) {
                text_Hum.text = getString(R.string.text_error)
            }
        })

        myData.child(uSer).child("Hum_Land").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                text_Hum_Land.text = snapshot.getValue(String::class.java) + " %"
            }

            override fun onCancelled(error: DatabaseError) {
                text_Hum_Land.text = "Lỗi"
            }
        })

        myData.child(uSer).child("Light").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                text_Light.text = snapshot.getValue(String::class.java)
            }

            override fun onCancelled(error: DatabaseError) {
                text_Light.text = "Lỗi"
            }
        })

        myData.child(uSer).child("Status_Pum").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.getValue(String::class.java) == "1") {
                    text_Status_Pum.text = "Bật"
                } else {
                    text_Status_Pum.text = "Tắt"
                }
            }

            override fun onCancelled(error: DatabaseError) {
                text_Status_Pum.text = "Lỗi"
            }
        })

        myData.child(uSer).child("Mode").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.getValue(String::class.java) == "1") {
                    text_Mode.text = "Chế Độ: Tự Động"
                    bt_Mode.text = "Chế Độ: Bằng Tay"
                    bt_Off.visibility = View.INVISIBLE
                    bt_On.visibility = View.INVISIBLE
                    Mode = "1"
                } else {
                    text_Mode.text = "Chế Độ: Bằng Tay"
                    bt_Mode.text = "Chế Độ: Tự Động"
                    bt_Off.visibility = View.VISIBLE
                    bt_On.visibility = View.VISIBLE
                    Mode = "0"
                }
            }

            override fun onCancelled(error: DatabaseError) {
                text_Mode.text = "Lỗi"
            }
        })

        bt_Mode.setOnClickListener {
            if (Mode === "1") {
                myData.child(uSer).child("Mode").setValue("0")
            } else {
                myData.child(uSer).child("Mode").setValue("1")
            }
        }

        bt_Off.setOnClickListener { myData.child(uSer).child("Pum").setValue("0") }

        bt_On.setOnClickListener { myData.child(uSer).child("Pum").setValue("1") }

        return root
    }
}