package com.example.smartagi.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.smartagi.R
import com.example.smartagi.TheWeatherActivity
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        root.giamSat.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_check)
        }

        root.thoiTiet.setOnClickListener{
//            Log.e("TAG", "setOnClickListener")
//            val myIntent = Intent(activity, TheWeatherActivity::class.java)
//            activity!!.startActivity(myIntent)
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_weather)
        }

        root.lich.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_calendar)
        }

        root.kyThuat.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_technical)
        }

        root.trongCay.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_plant_tree)
        }

        root.bonPhan.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_fertilize)
        }

        root.chamSoc.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_take_care)
        }

        root.sauBenh.setOnClickListener{
            val navController = Navigation.findNavController(root)
            navController.navigate(R.id.nav_pests)
        }

        return root
    }
}
