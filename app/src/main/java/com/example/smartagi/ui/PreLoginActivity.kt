package com.example.smartagi.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.example.smartagi.R
import com.example.smartagi.RegisterActivity
import com.example.smartagi.ui.login.LoginActivity

class PreLoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main__background__login)
        val login = findViewById<Button>(R.id.loginbg) as Button
        val register: Button = findViewById(R.id.register)
//        login.setOnClickListener{
//            Toast.makeText(this, "You clicked me.", Toast.LENGTH_SHORT).show()
//        }
        login.setOnClickListener {
//            loading.visibility = View.VISIBLE
            val changeActivity = Intent(this, LoginActivity::class.java)
            startActivity(changeActivity)
            Log.e("TAG", "Chage activity")
            //Complete and destroy login activity once successful
            finish()
        }
        register.setOnClickListener{
            val registerActivity = Intent(this, RegisterActivity::class.java)
            startActivity(registerActivity)
        }
    }
}