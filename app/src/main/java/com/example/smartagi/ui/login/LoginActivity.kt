package com.example.smartagi.ui.login

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.smartagi.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class LoginActivity : AppCompatActivity() {

    var database = FirebaseDatabase.getInstance()
    var myData = database.reference

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)
        val register: Button = findViewById(R.id.register)
        val forget_Pass: Button = findViewById(R.id.forget_pass)

        register.setOnClickListener{
            val registerActivity = Intent(this, RegisterActivity::class.java)
            startActivity(registerActivity)
        }

        forget_Pass.setOnClickListener{
            val forgetPassActivity = Intent(this, ForgetPass::class.java)
            startActivity(forgetPassActivity)
        }

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
//            val loginResult = it ?: return@Observer

            val dialog = Dialog(this)
            //dialog.setTitle(R.string.text_login_please_wait);
            //dialog.setTitle(R.string.text_login_please_wait);
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.activity_load)
            val text_Notification_Register: TextView = dialog.findViewById(R.id.display_Please_Wait)
            text_Notification_Register.setText(R.string.text_login_please_wait)
            myData.child(username.text.toString()).child("Pass").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.getValue(String::class.java) == password.text.toString()) {
                        Singleton.getInstance().userName = username.text.toString();
                        loading.visibility = View.GONE
                        dialog.cancel()
                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        loading.visibility = View.GONE
                        Notification(R.string.text_login_failed)
                        dialog.cancel()
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Notification(error.message)
                }
            })
            dialog.show()

//            loading.visibility = View.GONE
//            if (loginResult.error != null) {
//                showLoginFailed(loginResult.error)
//            }
//            if (loginResult.success != null) {
//                updateUiWithUser(loginResult.success)
//            }
//            setResult(Activity.RESULT_OK)
//            val changeActivity = Intent(this, MainActivity::class.java)
//            startActivity(changeActivity)
//            //Complete and destroy login activity once successful
//            finish()
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE
                loginViewModel.login(username.text.toString(), password.text.toString())
            }
        }
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun showLoginFailed(errorString: String) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun Notification(text_Error: Int) {
        val Notification_Error_Dialog: AlertDialog.Builder = AlertDialog.Builder(this)
        Notification_Error_Dialog.setTitle(text_Error)
        Notification_Error_Dialog.setCancelable(false)
        Notification_Error_Dialog.setNegativeButton(
            R.string.text_close,
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
        val alertDialog: AlertDialog = Notification_Error_Dialog.create()
        alertDialog.show()
    }

    private fun Notification(notification: String) {
        val Notification_Dialog: AlertDialog.Builder = AlertDialog.Builder(this)
        Notification_Dialog.setTitle(R.string.text_title)
        Notification_Dialog.setMessage(notification)
        Notification_Dialog.setCancelable(false)
        Notification_Dialog.setNegativeButton(
            R.string.text_close,
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
        val alertDialog: AlertDialog = Notification_Dialog.create()
        alertDialog.show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })

}