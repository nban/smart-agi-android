package com.example.smartagi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TechnicalFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TechnicalFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val database = FirebaseDatabase.getInstance()
        val myData = database.reference
        val root = inflater.inflate(R.layout.fragment_technical, container, false)
        val value_1: TextView = root.findViewById(R.id.id_text_value_1)
        val value_2: TextView = root.findViewById(R.id.id_text_value_2)
        val value_3: TextView = root.findViewById(R.id.id_text_value_3)
        val value_4: TextView = root.findViewById(R.id.id_text_value_4)
        val value_5: TextView = root.findViewById(R.id.id_text_value_5)
        val value_6: TextView = root.findViewById(R.id.id_text_value_6)

        myData.child("technical").child("value_1").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                value_1.text = snapshot.getValue(String::class.java)
            }

            override fun onCancelled(error: DatabaseError) {
                value_1.text = error.message
            }
        })

        myData.child("technical").child("value_2").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                value_2.text = snapshot.getValue(String::class.java)
            }

            override fun onCancelled(error: DatabaseError) {
                value_2.text = error.message
            }
        })

        myData.child("technical").child("value_3").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                value_3.text = snapshot.getValue(String::class.java)
            }

            override fun onCancelled(error: DatabaseError) {
                value_3.text = error.message
            }
        })

        myData.child("technical").child("value_4").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                value_4.text = snapshot.getValue(String::class.java)
            }

            override fun onCancelled(error: DatabaseError) {
                value_4.text = error.message
            }
        })

        myData.child("technical").child("value_5").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                value_5.text = snapshot.getValue(String::class.java)
            }

            override fun onCancelled(error: DatabaseError) {
                value_5.text = error.message
            }
        })

        myData.child("technical").child("value_6").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                value_6.text = snapshot.getValue(String::class.java)
            }

            override fun onCancelled(error: DatabaseError) {
                value_6.text = error.message
            }
        })

        return root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TechnicalFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TechnicalFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}