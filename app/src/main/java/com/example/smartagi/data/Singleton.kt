package com.example.smartagi.data

public class Singleton {
    private var instance: Singleton? = null

    private fun Singleton() {}

    fun getInstance(): Singleton? {
        if (instance == null) {
            synchronized(Singleton::class.java) {
                if (instance == null) {
                    instance = com.example.smartagi.data.Singleton()
                }
            }
        }
        return instance
    }
}