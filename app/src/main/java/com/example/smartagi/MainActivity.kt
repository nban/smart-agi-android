package com.example.smartagi

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_check,
                R.id.nav_calendar,
                R.id.nav_weather,
                R.id.nav_technical,
                R.id.nav_account,
                R.id.nav_contact,
                R.id.nav_fertilize,
                R.id.nav_pests,
                R.id.nav_plant_tree,
                R.id.nav_take_care
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

//        val btnLogout: Button = findViewById(R.id.btnLogout)
//        btnLogout.setOnClickListener(View.OnClickListener {
//            val registerActivity = Intent(this, LoginActivity::class.java)
//            startActivity(registerActivity)
//            finish()
//        })

//        navView.setNavigationItemSelectedListener {
//            when (it.itemId) {
//                R.id.nav_logout -> {
//                    val intent = Intent(this, LoginActivity::class.java)
//                    startActivity(intent)
//                    finish()
//                    true
//                }
//                else -> {
//                    appBarConfiguration = AppBarConfiguration(
//                        setOf(
//                            R.id.nav_home,
//                            R.id.nav_check,
//                            R.id.nav_calendar,
//                            R.id.nav_weather,
//                            R.id.nav_technical,
//                            R.id.nav_account,
//                            R.id.nav_contact,
//                            R.id.nav_fertilize,
//                            R.id.nav_pests,
//                            R.id.nav_plant_tree,
//                            R.id.nav_take_care
//                        ), drawerLayout
//                    )
//                    setupActionBarWithNavController(navController, appBarConfiguration)
//                    navView.setupWithNavController(navController)
//                    false
//                }
//            }
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}