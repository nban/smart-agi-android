package com.example.smartagi;

public class Singleton {

    private static Singleton instance = null;
    public String userName = null;
    private Singleton() {

    }

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
