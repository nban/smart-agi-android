package com.example.smartagi;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myData = database.getReference();

    String eMail, uSer, number_Phone, passWord, passWord_Repeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button bt_Huy = findViewById(R.id.bt_Cancel_Register);
        Button bt_Oke = findViewById(R.id.bt_Logout);
        EditText inPut_User = findViewById(R.id.inPut_User_Register);
        EditText inPut_Number_Phone = findViewById(R.id.inPut_Number_Phone_Register);
        EditText inPut_Pass = findViewById(R.id.pass_2);
        EditText inPut_Pass_Repeat = findViewById(R.id.pass_3);

        bt_Oke.setOnClickListener(v -> {
            uSer = inPut_User.getText().toString().trim();
            if (uSer.length() > 0) {
                number_Phone = inPut_Number_Phone.getText().toString().trim();
                if (number_Phone.length() > 9) {
                    passWord = inPut_Pass.getText().toString();
                    passWord_Repeat = inPut_Pass_Repeat.getText().toString();
                    if (passWord.length() > 5 & passWord_Repeat.length() > 5) {
                        if (passWord.equals(passWord_Repeat)) {
                            Register_Online();
                        } else {
                            Notification_Register(R.string.text_pass_repeat_format);
                        }
                    } else {
                        Notification_Register(R.string.text_pass_format);
                    }
                } else {
                    Notification_Register(R.string.text_number_phone_format);
                }
            } else {
                Notification_Register(R.string.text_user_error);
            }
        });

        bt_Huy.setOnClickListener(v -> finish());
    }

    private void Register_Online() {
        final boolean[] check = {true};
        final Dialog dialog = new Dialog(this);
        //dialog.setTitle(R.string.text_register_please_wait);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.activity_load);
        TextView text_Notification_Register = dialog.findViewById(R.id.display_Please_Wait);
        text_Notification_Register.setText(R.string.text_register_please_wait);
        myData.child(uSer).child("register").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.getValue(Boolean.class) == null){
                    check[0] = false;
                    myData.child(uSer).child("Hum").setValue("--.-");
                    myData.child(uSer).child("Hum_Land").setValue("--");
                    myData.child(uSer).child("Light").setValue("---");
                    myData.child(uSer).child("Pum").setValue("0");
                    myData.child(uSer).child("Status_Pum").setValue("0");
                    myData.child(uSer).child("Temp").setValue("--.-");
                    myData.child(uSer).child("register").setValue(true);
                    myData.child(uSer).child("Phone").setValue(number_Phone);
                    myData.child(uSer).child("Pass").setValue(passWord);
                    myData.child(uSer).child("Mode").setValue("1");
                    myData.child(uSer).child("High").setValue("70");
                    myData.child(uSer).child("Low").setValue("30");
                    Data_Save("User", uSer);
                    Data_Save("Pass_Word", passWord);
                    Data_Save("Save_Login", true);
                    Notification_Register(uSer, passWord, number_Phone);
                    dialog.cancel();
                } else if(check[0]){
                    Notification_Register(R.string.text_register_failed);
                    dialog.cancel();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Notification_Register(error.getMessage());
            }
        });

        dialog.show();
    }

    private void Notification_Register(Integer text_Error) {
        AlertDialog.Builder Notification_Error_Dialog = new AlertDialog.Builder(this);
        Notification_Error_Dialog.setTitle(text_Error);
        Notification_Error_Dialog.setCancelable(false);
        Notification_Error_Dialog.setNegativeButton(R.string.text_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = Notification_Error_Dialog.create();
        alertDialog.show();
    }

    private void Notification_Register(String notification) {
        AlertDialog.Builder Notification_Dialog = new AlertDialog.Builder(this);
        Notification_Dialog.setTitle(R.string.text_title);
        Notification_Dialog.setMessage(notification);
        Notification_Dialog.setCancelable(false);
        Notification_Dialog.setNegativeButton(R.string.text_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = Notification_Dialog.create();
        alertDialog.show();
    }

    private void Notification_Register(String text_User, String text_Pass, String text_Phone) {
        AlertDialog.Builder Notification_Error_Dialog = new AlertDialog.Builder(this);
        Notification_Error_Dialog.setTitle(R.string.text_register_successful);
        Notification_Error_Dialog.setMessage(getString(R.string.text_info_login, text_User, text_Pass, text_Phone));
        Notification_Error_Dialog.setCancelable(false);
        Notification_Error_Dialog.setNegativeButton(R.string.text_login_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                // đăng nhập ngay
                finish();
                dialog.cancel();
            }
        });
        Notification_Error_Dialog.setPositiveButton(R.string.text_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = Notification_Error_Dialog.create();
        alertDialog.show();
    }

    public void Data_Save(String key, Boolean data){
        SharedPreferences save_Data = getSharedPreferences("app_info", MODE_PRIVATE);
        SharedPreferences.Editor edit_Save_Data = save_Data.edit();
        edit_Save_Data.putBoolean(key, data);
        edit_Save_Data.apply();
        edit_Save_Data.commit();
    }

    public void Data_Save(String key, String data){
        SharedPreferences save_Data = getSharedPreferences("app_info", MODE_PRIVATE);
        SharedPreferences.Editor edit_Save_Data = save_Data.edit();
        edit_Save_Data.putString(key, data);
        edit_Save_Data.apply();
        edit_Save_Data.commit();
    }

    public String Read_Data_String(String key){
        SharedPreferences save_Data = getSharedPreferences("app_info", MODE_PRIVATE);
        return save_Data.getString(key,"");
    }

    public Boolean Read_Data_Boolean(String key){
        SharedPreferences save_Data = getSharedPreferences("app_info", MODE_PRIVATE);
        return save_Data.getBoolean(key,false);
    }
}
