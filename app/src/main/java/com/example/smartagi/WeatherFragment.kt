package com.example.smartagi

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import org.json.JSONObject
import java.net.URL
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WeatherFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WeatherFragment : Fragment() {




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_weather, container, false)
//        val toolbar = view.findViewById<View>(R.id.toolbar) as Toolbar
//        setSupportActionBar(toolbar)
//        toolbar.setNavigationIcon(R.drawable.ic_menu_exit)
//        toolbar.setNavigationOnClickListener {
//            onBackPressed()
//        }
        return view
    }

    //            val CITY: String = "Ho Chi Minh City, VN"
    private var CITY: String? = null
    val API: String = "8118ed6ee68db2debfaaa5a44c832918"

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    protected var mLastLocation: Location? = null
    private var mLatitudeLabel: Double? = null
    private var mLongitudeLabel: Double? = null

    companion object {
        private val TAG = "LocationProvider"
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFusedLocationClient = activity?.let { LocationServices.getFusedLocationProviderClient(it) }


    }

    public override fun onStart() {
        super.onStart()

        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
        }
    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     *
     *
     * Note: this method should be called after location permission has been granted.
     */
    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        activity?.let {
            mFusedLocationClient!!.lastLocation
                .addOnCompleteListener(it) { task ->
                    val any = if (task.isSuccessful && task.result != null) {
                        mLastLocation = task.result
                        mLatitudeLabel = (mLastLocation)!!.latitude
                        mLongitudeLabel = (mLastLocation)!!.longitude

                        Log.e(TAG, "mLatitudeLabel :   $mLatitudeLabel")
                        Log.e(TAG, "mLongitudeLabel :   $mLongitudeLabel")

                        val geocoder = Geocoder(activity, Locale.getDefault())
                        val addresses: List<Address> =
                            geocoder.getFromLocation(mLatitudeLabel!!, mLongitudeLabel!!, 1)
                        try {
                            Log.e(TAG, "Dia chi:  $addresses")
                            val cityName: String = addresses[0].getAddressLine(0)
                            Log.e(TAG, "Dia chi:  $cityName")
                            val adminArea: String = addresses[0].adminArea
                            Log.e(TAG, "Dia chi:  $adminArea")
                            val name = adminArea
    //                        val exp: String = addresses[0].subAdminArea
                            Log.e(TAG, name)
                            CITY = URLEncoder.encode( name, "UTF-8");
                            Log.e(TAG, "Dia chi:  $CITY")
                            if (CITY != null) {
                                weatherTask().execute()
                            } else {
                                Toast.makeText(activity, "Không có tên thành phố", Toast.LENGTH_LONG).show()
                            }
                        } catch (ex: Exception) {
                            Log.e(TAG, ex.toString())
                        }

                    } else {
                        CITY = "Ho Chi Minh City, VN"
                        weatherTask().execute()
                        Log.w(TAG, "getLastLocation:exception", task.exception)
                    }
                }
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        val permissionState = activity?.let {
            ActivityCompat.checkSelfPermission(
                it,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        }
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        activity?.let {
            ActivityCompat.requestPermissions(
                it,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    private fun requestPermissions() {
        val shouldProvideRationale = activity?.let {
            ActivityCompat.shouldShowRequestPermissionRationale(
                it,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        }

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale!!) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation()
            } else {
                Log.e(TAG, "else getLastLocation")
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class weatherTask() : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            /* Showing the ProgressBar, Making the main design GONE */
            view!!.findViewById<ProgressBar>(R.id.loader).visibility = View.VISIBLE
            view!!.findViewById<RelativeLayout>(R.id.mainContainer).visibility = View.GONE
            view!!.findViewById<TextView>(R.id.errorText).visibility = View.GONE
        }

        override fun doInBackground(vararg params: String?): String? {
            var response: String?
            response = try {
                URL("https://api.openweathermap.org/data/2.5/weather?q=$CITY&units=metric&appid=$API").readText(
                    Charsets.UTF_8
                )
            } catch (e: Exception) {
                CITY = "Ho Chi Minh"
                weatherTask().execute()
                Log.e(TAG,"EEEEEEEEEEEEEEE")
                Log.e(TAG, e.toString())
                null
            }
            return response
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                /* Extracting JSON returns from the API */
                val jsonObj = JSONObject(result)
                val cod = jsonObj.getInt("cod")
                if (cod == 200) {
                    val main = jsonObj.getJSONObject("main")
                    val sys = jsonObj.getJSONObject("sys")
                    val wind = jsonObj.getJSONObject("wind")
                    val weather = jsonObj.getJSONArray("weather").getJSONObject(0)

                    val updatedAt: Long = jsonObj.getLong("dt")
                    val updatedAtText =
                        "Updated at: " + SimpleDateFormat(
                            "dd/MM/yyyy hh:mm a",
                            Locale.ENGLISH
                        ).format(
                            Date(updatedAt * 1000)
                        )
                    val temp = main.getString("temp") + "°C"
                    val tempMin = "Min Temp: " + main.getString("temp_min") + "°C"
                    val tempMax = "Max Temp: " + main.getString("temp_max") + "°C"
                    val pressure = main.getString("pressure")
                    val humidity = main.getString("humidity")

                    val sunrise: Long = sys.getLong("sunrise")
                    val sunset: Long = sys.getLong("sunset")
                    val windSpeed = wind.getString("speed")
                    val weatherDescription = weather.getString("description")

                    val address = jsonObj.getString("name") + ", " + sys.getString("country")

                    /* Populating extracted data into our views */
                    view!!.findViewById<TextView>(R.id.address).text = address
                    view!!.findViewById<TextView>(R.id.updated_at).text = updatedAtText
                    view!!.findViewById<TextView>(R.id.status).text = weatherDescription.capitalize()
                    view!!.findViewById<TextView>(R.id.temp).text = temp
                    view!!.findViewById<TextView>(R.id.temp_min).text = tempMin
                    view!!.findViewById<TextView>(R.id.temp_max).text = tempMax
                    view!!.findViewById<TextView>(R.id.sunrise).text =
                        SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(Date(sunrise * 1000))
                    view!!.findViewById<TextView>(R.id.sunset).text =
                        SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(Date(sunset * 1000))
                    view!!.findViewById<TextView>(R.id.wind).text = windSpeed
                    view!!.findViewById<TextView>(R.id.pressure).text = pressure
                    view!!.findViewById<TextView>(R.id.humidity).text = humidity

                    /* Views populated, Hiding the loader, Showing the main design */
                    view!!.findViewById<ProgressBar>(R.id.loader).visibility = View.GONE
                    view!!.findViewById<RelativeLayout>(R.id.mainContainer).visibility = View.VISIBLE
                    view!!.findViewById<TextView>(R.id.errorText).visibility = View.GONE
                }
                if (cod.toString() == "404") {
                    CITY = "HO CHI MINH"
                    weatherTask().execute()
                }
            } catch (e: Exception) {
                Log.e(TAG, e.toString())
                view!!.findViewById<ProgressBar>(R.id.loader).visibility = View.GONE
                view!!.findViewById<TextView>(R.id.errorText).visibility = View.VISIBLE
            }
        }
    }
}